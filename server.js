const express = require('express');
const app = express();
const path = require('path');
const pug = require('pug');

const port = process.env.PORT || 3000

//console.log(pug.renderFile('./views/index.pug'));
//pug.compileFile('./views/index.pug');
//pug.renderFile('./views/index.pug')
app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({ extended: true }))
//Sirve parsear el body
//Para poder extraer los datos del formulario
//que se envian en el cuerpor de la solicitud POST


app.set('view engine', 'pug');

app.get("/", function(req, res){
    res.render('index')
});

app.get("/about", function(req, res){
    res.render('about')
});

app.get("/product", function(req, res){
    res.render('product')
});

app.get("/service", function(req, res){
    res.render('service')
});

app.post("/form",
    function(req, res){
        const {name, lastname, day, month, year, email, phone, message} = req.body

        const isUserName = () => {
            const expRegNombre=/^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
            if(expRegNombre.test(name)){
                /*res.render(path.join(__dirname + '/views/contactmess.pug'),{
                    nameError: "Nombre valido";
                    nerror: 'bien';
                    name: name;
                })*/
                return true
            }else{
                /*res.render(path.join(__dirname + '/views/service.pug'),{
                    nameError: "Nombre invalido";
                    nerror: 'error';
                })*/
                return false
            }
        }

        const isUserLastName = () => {
            const expRegApellidos=/^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
            if(expRegApellidos.test(lastname)){
                /*res.render(path.join(__dirname + '/views/contactmess.pug'),{
                    lastnameError: "Apellido valido",
                    lerror: 'bien',
                    lastname: lastname,
                })*/
                return true
            }else{
                /*res.render(path.join(__dirname + '/views/service.pug'),{
                    lastnameError: "Apellido invalido",
                    lerror: 'error'
                })*/
                return false
            }
        }

        const isEmail = () => {
          const expRegCorreo=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
          if (expRegCorreo.test(email)){
            /*res.render(path.join(__dirname + '/views/contactmess.pug'),{
                emailError: "Correo valido",
                gerror: 'bien',
                email: email,
            })*/
            return true
          }else{
            /*res.render(path.join(__dirname + '/views/service.pug'),{
                emailError: "Correo invalido",
                gerror: 'error',
            })*/
            return false
          }
        }

        const isPhone = () => {
            if(!(/^\d{9}$/.test(phone)) ) {
                /*res.render(path.join(__dirname + '/views/service.pug'),{
                    phoneError: "Telefono invalido",
                    perror: 'error',
                })*/
                return false;
            }else{
                /*res.render(path.join(__dirname + '/views/contactmess.pug'),{
                    phoneError: "Telefono valido",
                    perror: 'bien',
                    phone: phone,
                })*/
                return true
            }
        }

        const isMessage = () => {
            const mincaracteres = 150;
            if (message.length < mincaracteres){
                /*res.render(path.join(__dirname + '/views/service.pug'),{
                    messageError: "El mensaje debe tener como minimo 150 caracteres",
                    merror: 'error',
                })*/
                return false
            }else{
                /*res.render(path.join(__dirname + '/views/contactmess.pug'),{
                    messageError: "Mensaje valido",
                    merror: 'bien',
                    message: message,
                })*/
                return true
            }
        }

        const isDay = () => {
            if (day <= 31 && day > 0) {
                /*res.render(path.join(__dirname + '/views/contactmess.pug'),{
                    dayError: "Dia valido",
                    derror: 'bien',
                    day: day,
                })*/
                return true
            }else{
                /*res.render(path.join(__dirname + '/views/service.pug'),{
                    dayError: "Dia invalido",
                    derror: 'error',
                })*/
                return false
            }
        }

        const isMonth = () => {
            if (month <= 12 && month > 0) {
                /*res.render(path.join(__dirname + '/views/contactmess.pug'),{
                    monthError: "Mes valido",
                    monerror: 'bien',
                    month: month,
                })*/
                return true
            }else{
                /*res.render(path.join(__dirname + '/views/service.pug'),{
                    monthError: "Mes invalido",
                    monerror: 'error',
                })*/
                return false
            }
        }

        const isYear = () => {
            if (year <= 2020 && year >= 1940) {
                /*res.render(path.join(__dirname + '/views/contactmess.pug'),{
                    yearError: "Año valido",
                    yerror: 'bien',
                    year: year,
                })*/
                return true
            }else{
                /*res.render(path.join(__dirname + '/views/service.pug'),{
                    yearError: "Año invalido",
                    yerror: 'error',
                })*/
                return false
            }
        }

        const nameValidated = isUserName()
        const lastnameValidated = isUserLastName()
        const emailValidated = isEmail()
        const phoneValidated = isPhone()
        const messageValidated = isMessage()
        const dayValidated = isDay()
        const monthValidated = isMonth()
        const yearValidated = isYear()

        if(nameValidated){
            if(lastnameValidated){
                if(emailValidated){
                    if(phoneValidated){
                        if(messageValidated){
                            if(dayValidated){
                                if(monthValidated){
                                    if(yearValidated){
                                        res.render(path.join(__dirname + '/views/contactmess.pug'), {
                                            name: name,
                                            lastname: lastname,
                                            day: day,
                                            month: month,
                                            year: year,
                                            email: email,
                                            phone: phone,
                                            message: message,
                                            name2: "Mis",
                                            lastname2: "Huevos Morenos"
                                        })
                                        console.log("Todo bien")
                                    }else{
                                        res.render(path.join(__dirname + '/views/service.pug'), {
                                            nameError: "Nombre valido", nerror: 'bien',
                                            lastnameError: "Apellido valido", lerror: 'bien',
                                            emailError: "Correo valido", gerror: 'bien',
                                            phoneError: "Telefono valido", perror: 'bien',
                                            messageError: "Mensaje valido", merror: 'bien',
                                            dayError: "Dia valido", derror: 'bien',
                                            monthError: "Mes valido", monerror: 'bien',
                                            yearError: "Año invalido", yerror: 'error',
                                            name: name, lastname: lastname, day: day, month: month, year: year, email: email, phone: phone, message: message,
                                        })
                                        console.log("Mal")
                                    }
                                }else{
                                    res.render(path.join(__dirname + '/views/service.pug'), {
                                        nameError: "Nombre valido", nerror: 'bien',
                                        lastnameError: "Apellido valido", lerror: 'bien',
                                        emailError: "Correo valido", gerror: 'bien',
                                        phoneError: "Telefono valido", perror: 'bien',
                                        messageError: "Mensaje valido", merror: 'bien',
                                        dayError: "Dia valido", derror: 'bien',
                                        monthError: "Mes invalido", monerror: 'error',
                                        name: name, lastname: lastname, day: day, month: month, year: year, email: email, phone: phone, message: message,
                                    })
                                    console.log("Mal")
                                }
                            }else{
                                res.render(path.join(__dirname + '/views/service.pug'), {
                                    nameError: "Nombre valido", nerror: 'bien',
                                    lastnameError: "Apellido valido", lerror: 'bien',
                                    emailError: "Correo valido", gerror: 'bien',
                                    phoneError: "Telefono valido", perror: 'bien',
                                    messageError: "Mensaje valido", merror: 'bien',
                                    dayError: "Dia invalido", derror: 'error',
                                    name: name, lastname: lastname, day: day, month: month, year: year, email: email, phone: phone, message: message,
                                })
                                console.log("Mal")
                            }
                        }else{
                            res.render(path.join(__dirname + '/views/service.pug'), {
                                nameError: "Nombre valido", nerror: 'bien',
                                lastnameError: "Apellido valido", lerror: 'bien',
                                emailError: "Correo valido", gerror: 'bien',
                                phoneError: "Telefono valido", perror: 'bien',
                                messageError: "El mensaje debe tener como minimo 150 caracteres", merror: 'error',
                                name: name, lastname: lastname, day: day, month: month, year: year, email: email, phone: phone, message: message,
                            })
                            console.log("Mal")
                        }
                    }else{
                        res.render(path.join(__dirname + '/views/service.pug'), {
                            nameError: "Nombre valido", nerror: 'bien',
                            lastnameError: "Apellido valido", lerror: 'bien',
                            emailError: "Correo valido", gerror: 'bien',
                            phoneError: "Telefono invalido", perror: 'error',
                            name: name, lastname: lastname, day: day, month: month, year: year, email: email, phone: phone, message: message,
                        })
                        console.log("Mal")
                    }
                }else{
                    res.render(path.join(__dirname + '/views/service.pug'), {
                        nameError: "Nombre valido", nerror: 'bien',
                        lastnameError: "Apellido valido", lerror: 'bien',
                        emailError: "Correo invalido", gerror: 'error',
                        name: name, lastname: lastname, day: day, month: month, year: year, email: email, phone: phone, message: message,
                    })
                    console.log("Mal")
                }
            }else{
                res.render(path.join(__dirname + '/views/service.pug'), {
                    nameError: "Nombre valido", nerror: 'bien',
                    lastnameError: "Apellido invalido", lerror: 'error',
                    name: name, lastname: lastname, day: day, month: month, year: year, email: email, phone: phone, message: message,
                })
                console.log("Mal")
            }
        }else{
            res.render(path.join(__dirname + '/views/service.pug'), {
                nameError: "Nombre invalido", nerror: 'error',
                name: name, lastname: lastname, day: day, month: month, year: year, email: email, phone: phone, message: message,
            })
            console.log("Mal")
        }
    });

app.listen(port, () => console.log(`Escuchando tus peticiones en el puerto ${port}`));